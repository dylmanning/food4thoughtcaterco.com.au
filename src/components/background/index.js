import { h, Component } from 'preact'
import classNames from 'classnames'
import style from './style'

const FALLABACK_IMAGE = './assets/images/home@fallback.jpg'
const REGULAR_IMAGE = './assets/images/home.jpg'
const HIGHRES_IMAGE = './assets/images/home@retina.jpg'
const HIGHRES_QUERY = '(-webkit-min-device-pixel-ratio: 2), (min-device-pixel-ratio: 2), (min-resolution: 192dpi)'

export default class Background extends Component {
  constructor (props) {
    super(props)

    this.state = {
      windowSize: {},
      backgroundSrc: '',
      backgroundLoaded: false
    }

    this.imageReady = this.imageReady.bind(this)
    this.resizeBackground = this.resizeBackground.bind(this)
    this.determineScreen = this.determineScreen.bind(this)
  }

  componentWillMount () {
    this.determineScreen()
  }

  componentDidMount () {
    this.resizeBackground()
    window.addEventListener('resize', this.resizeBackground)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resizeBackground)
  }

  imageReady () {
    if (!this.state.backgroundLoaded) {
      this.setState({ backgroundLoaded: true })
    }
  }

  resizeBackground () {
    this.setState({ windowSize: {
      height: window.innerHeight,
      width: window.innerWidth
    }})
  }

  determineScreen () {
    if (typeof window !== 'undefined') {
      if (window.matchMedia(HIGHRES_QUERY).matches) {
        this.setState({ backgroundSrc: HIGHRES_IMAGE })
      } else {
        this.setState({ backgroundSrc: REGULAR_IMAGE })
      }
    }
  }

  render () {
    const loaded = this.state.backgroundLoaded
    const visibility = loaded ? {} : style.hidden
    return (
      <div class={style.wrapper}>
        {!loaded && <img style={this.state.windowSize} class={style.background} src={FALLABACK_IMAGE} alt='background' />}
        <img style={this.state.windowSize} class={classNames(style.background, visibility)} src={this.state.backgroundSrc} onLoad={this.imageReady} alt='background' />
      </div>
    )
  }
}
