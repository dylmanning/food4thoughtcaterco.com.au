import { h } from 'preact'
import style from './style'

const Logo = ({ image }) => {
  return (
    <div class={style.logo}>
      <img src={image} alt='food4thoughtcaterco' />
    </div>
  )
}

export default Logo
