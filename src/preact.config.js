import criticalCssPlugin from 'preact-cli-plugin-critical-css'

export default (config, env) => {
  criticalCssPlugin(config, env)
}
