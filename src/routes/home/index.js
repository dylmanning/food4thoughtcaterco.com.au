import { h, Component } from 'preact'
import style from './style'

/* eslint-disable */
import Logo from 'async!../../components/logo'
import Background from 'async!../../components/background'
/* eslint-enable */

export default class Home extends Component {
  render () {
    return (
      <div>
        <Background />
        <div class={style.home}>
          <div class={style.banner}>
            <embed src='./assets/images/comingsoon.svg' />
          </div>
          <Logo image='./assets/images/logo.png' />
        </div>
      </div>
    )
  }
}
